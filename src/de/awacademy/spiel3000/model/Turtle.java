package de.awacademy.spiel3000.model;

public class Turtle {

    private double posX;
    private double posY;

    public Turtle(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }
}
