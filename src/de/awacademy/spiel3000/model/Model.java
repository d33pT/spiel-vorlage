package de.awacademy.spiel3000.model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private int counter = 0;
    private List<Turtle>turtles = new LinkedList<>();

    public void newTurtle (double posX, double posY){
        turtles.add(new Turtle(posX,posY));
    }

    public int getCounter(){
        return counter;
    }

    public List<Turtle> getTurtles(){
        return turtles;
    }

    public void update(long deltaMillis){
        counter += deltaMillis;
    }

    public void resetCounter() {
        counter = 0;
    }
}
