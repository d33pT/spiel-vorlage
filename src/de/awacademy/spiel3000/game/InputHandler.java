package de.awacademy.spiel3000.game;

import de.awacademy.spiel3000.model.Model;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class InputHandler {
    private Model model;

    public InputHandler(Model model){
        this.model = model;
    }

    public void onKeyPresses(KeyEvent event){

    }

    public void onKeyReleased(KeyEvent event) {
        if (event.getCode() == KeyCode.SPACE){
            this.model.resetCounter();
        }
    }

    public void onClick(MouseEvent event){
        model.newTurtle(event.getX(), event.getY());
    }
}
