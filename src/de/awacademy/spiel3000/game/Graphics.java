package de.awacademy.spiel3000.game;

import de.awacademy.spiel3000.model.Model;
import de.awacademy.spiel3000.model.Turtle;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Graphics {

    private Model model;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    private GraphicsContext gc;

    public void draw() {
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0,1200,750);

        gc.setFill(Color.WHITE);
        gc.fillText("Hallo Welt" + model.getCounter(), 30,30);

        gc.setFill(Color.DARKTURQUOISE);
        for (Turtle turtle : model.getTurtles()){
            gc.fillRect(turtle.getPosX() - 40, turtle.getPosY() - 40,80,80);
        }
    }
}
